hmi_continuum = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,4}__SDO_HMI_HMI_continuum.jp2',
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_HMI_HMI_continuum.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/HMI/%Y/%m/%d/continuum/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 32,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'hmi_continuum',
}

hmi_magnetogram = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,4}__SDO_HMI_HMI_magnetogram.jp2',
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_HMI_HMI_magnetogram.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/HMI/%Y/%m/%d/magnetogram/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 32,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'hmi_magnetogram',
}

aia_171 = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,4}__SDO_AIA_AIA_171.jp2',
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_AIA_AIA_171.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/AIA/%Y/%m/%d/171/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 32,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'aia_171',
}

aia_304 = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,4}__SDO_AIA_AIA_304.jp2',
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{1,4})__SDO_AIA_AIA_304.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/AIA/%Y/%m/%d/304/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 32,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'aia_304',
}

gong_halpha = {
    'filename_pattern'           : r'[0-9]{14}[A-Z][a-z].fits.fz',
    'filename_date_pattern'      : r'([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})[A-Z][a-z].fits.fz',
    'base_url_format'            : "ftp://gong2.nso.edu/HA/haf/%Y%m/%Y%m%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                   : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'gong_halpha',
}

gong_magnetogram = {
    'filename_pattern'           : r'[a-z]{5}[0-9]{6}[a-z][0-9]{4}.fits.gz',
    'filename_date_pattern'      : r'[a-z]{5}([0-9]{2})([0-9]{2})([0-9]{2})[a-z]([0-9]{2})([0-9]{2}).fits.gz',
    'base_url_format'            : "ftp://gong2.nso.edu/oQR/zqa/%Y%m/%%station%%zqa%y%m%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 2000,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'gong_magnetogram',
}

gong_farside = {
    'filename_pattern'           : r'mrfqo[0-9]{2}[0-9]{2}[0-9]{2}t[0-9]{2}[0-9]{2}.fits',
    'filename_date_pattern'      : r'mrfqo([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2}).fits',
    'base_url_format'            : "http://farside.nso.edu/oQR/fqo/%Y%m/mrfqo%y%m%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                   : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'gong_farside',
}

solis_v22 = {
    'filename_pattern'           : r'k4v22[0-9]{6}t[0-9]{6}.fts.gz',
    'filename_date_pattern'      : r'k4v22([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
    'base_url_format'            : "http://solis.nso.edu/pubkeep/v22/%Y%m/k4v22%y%m%d",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 2000,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'solis_v22',
}

solis_v82 = {
    'filename_pattern'           : r'k4v82[0-9]{6}t[0-9]{6}.fts.gz',
    'filename_date_pattern'      : r'k4v82([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
    'base_url_format'            : "http://solis.nso.edu/pubkeep/v82/%Y%m/k4v82%y%m%d",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 2000,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'solis_v82',
}

solis_v95 = {
    'filename_pattern'           : r'k4v95[0-9]{6}t[0-9]{6}.fts.gz',
    'filename_date_pattern'      : r'k4v95([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
    'base_url_format'            : "http://solis.nso.edu/pubkeep/v95/%Y%m/k4v95%y%m%d",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 2000,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'solis_v95',
}

HMI_continuum_jp2 = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}__SDO_HMI_HMI_continuum.jp2', 
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{2})__SDO_HMI_HMI_continuum.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/HMI/%Y/%m/%d/continuum/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'HMI_continuum_jp2',
}

HMI_magnetogram_jp2 = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}__SDO_HMI_HMI_magnetogram.jp2', 
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})_([0-9]{2})__SDO_HMI_HMI_magnetogram.jp2',
    'base_url_format'            : "http://sdowww.lmsal.com/sdomedia/hv_jp2kwrite/v0.8/jp2/HMI/%Y/%m/%d/magnetogram/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'HMI_continuum_jp2',
}

callisto = {
    'filename_pattern'           : r'(?:ALASKA|ALMATY|BIR|BLEN7M|BLENSW|GAURI|HUMAIN|MRT1|MRT2|MRT3|RCAG|ROSWELL-NM|SSRT|TRIEST)_[0-9]{4}[0-9]{2}[0-9]{2}_[0-9]{2}[0-9]{2}[0-9]{2}_[0-9]{2}.fit.gz',
    'filename_date_pattern'      : r'.*_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_[0-9]{2}.fit.gz',
    'base_url_format'            : "http://soleil.i4ds.ch/solarradio/data/2002-20yy_Callisto/%Y/%m/%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'callisto',
}

lyra = {
    'filename_pattern'           : r'lyra_[0-9]{4}[0-9]{2}[0-9]{2}-[0-9]{2}[0-9]{2}[0-9]{2}_lev3_std.fits',
    'filename_date_pattern'      : r'lyra_([0-9]{4})([0-9]{2})([0-9]{2})-([0-9]{2})([0-9]{2})([0-9]{2})_lev3_std.fits',
    'base_url_format'            : "http://proba2.oma.be/lyra/data/bsd/%Y/%m/%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'lyra',
}

swap = {
    'filename_pattern'           : r'[0-9]{4}_[0-9]{2}_[0-9]{2}__[0-9]{2}_[0-9]{2}_[0-9]{2}__PROBA2_SWAP_SWAP_174.jp2',
    'filename_date_pattern'      : r'([0-9]{4})_([0-9]{2})_([0-9]{2})__([0-9]{2})_([0-9]{2})_([0-9]{2})__PROBA2_SWAP_SWAP_174.jp2',
    'base_url_format'            : "http://proba2.oma.be/swap/data/qlk/%Y/%m/%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 32,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'swap',
}

rob_uset = {
    'filename_pattern'           : r'UCH[0-9]{4}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}.FTS',
    'filename_date_pattern'      : r'UCH([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2}).FTS',
    'base_url_format'            : "http://sidc.be/DATA/uset/Halpha/%Y/%m/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'rob_uset',
}

kanzelhoehe = {
    'filename_pattern'           : r'kanz_halph_fc_[0-9]{4}[0-9]{2}[0-9]{2}_[0-9]{2}[0-9]{2}[0-9]{2}.fts.gz',
    'filename_date_pattern'      : r'kanz_halph_fc_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2}).fts.gz',
    'base_url_format'            : "https://www.kso.ac.at/halpha4M/FITS/normal/%Y/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'kanzelhoehe',
}

nrh = {
    'filename_pattern'           : r'nrh2_1509_h80_[0-9]{4}[0-9]{2}[0-9]{2}_[0-9]{2}[0-9]{2}[0-9]{2}c05_q.fts',
    'filename_date_pattern'      : r'nrh2_1509_h80_([0-9]){4}([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})c05_q.fts',
    'base_url_format'            : "http://swhv.oma.be/test/nrh2_1509_h80_20140418_082157c05_q.fts",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'nrh',
}

gong_pfss = {
    'filename_pattern'           : r'mrbqs[0-9]{2}[0-9]{2}[0-9]{2}t[0-9]{2}[0-9]{2}c[0-9]{4}_[0-9]{3}.fits.gz',
    'filename_date_pattern'      : r'mrbqs([0-9]{2})([0-9]{2})([0-9]{2})t([0-9]{2})([0-9]{2})c[0-9]{4}_[0-9]{3}.fits.gz',
    'base_url_format'            : "https://gong.nso.edu/data/magmap/QR/bqs/%Y%m/mrbqs%y%m%d/",
    'output_dir_date_format_add' : "/%Y/%m/%d",
    'add_to_year'                : 0,
    'limitjobs'                  : 8,
    'checkexist'                 : True,
    'create_directory'           : True,
    'db_keyword'                 : 'gong_pfss',
}


config = {
    'aia_171'            : aia_171,
    'aia_304'            : aia_304,
    'hmi_continuum'      : hmi_continuum,
    'hmi_magnetogram'    : hmi_magnetogram,
    'gong_halpha'        : gong_halpha,
    'gong_magnetogram'   : gong_magnetogram,
    'gong_farside'       : gong_farside,
    'gong_pfss'          : gong_pfss,
    'solis_v22'          : solis_v22,
    'solis_v82'          : solis_v82,
    'solis_v95'          : solis_v95,
    'HMI_continuum_jp2'  : HMI_continuum_jp2,
    'HMI_magnetogram_jp2': HMI_magnetogram_jp2,
    'callisto'           : callisto,
    'swap'               : swap,
    'lyra'               : lyra,
    'rob_uset'           : rob_uset,
    'kanzelhoehe'        : kanzelhoehe,
    'nrh'                : nrh,
}
