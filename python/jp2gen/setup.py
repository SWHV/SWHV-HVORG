#!/usr/bin/env python
from setuptools import setup, find_packages

scripts = ['bin/jp2download.py' ]

kwargs = {'name': 'jp2gen',
          'description': 'Download tools',
          'author': 'ROB',
          'packages': find_packages(),
          'scripts': scripts,
         }

instllrqrs = []
kwargs['install_requires'] = instllrqrs

clssfrs = ["Programming Language :: Python",
           "Programming Language :: Python :: 2.7",
           "Development Status :: 5 - Production/Stable",
           "Intended Audience :: Science/Research",
           "Intended Audience :: Information Technology",
           "Topic :: Software Development :: Libraries :: Python Modules"]
kwargs['classifiers'] = clssfrs
kwargs['version'] = '0.1'

setup(**kwargs)
