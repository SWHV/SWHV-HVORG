import os
import re
import datetime
import shutil
import logging

class DateFile():
    def __init__(self,date,path, outputfile, url):
        self.date = date
        self.path = path
        self.outputfile = outputfile
        self.url = url
    def __repr__(self):
        return str(self.date) + " "+ str(self.path) + "\n"
def parse_date(date):
    lend = len(date)
    if lend>=10:
        year = int(date[0:4])
        month = int(date[5:7])
        day = int(date[8:10])
    if lend<16:
        return datetime.datetime(year, month,day)
    if lend>=16:
        hour = int(date[11:13])
        minute = int(date[14:16])
    if lend<19:
        return datetime.datetime(year, month,day, hour, minute)
    if lend>=19:
        seconds = int(date[17:19])
        return datetime.datetime(year, month,day, hour, minute, seconds)

def sort_date_files(date_file_list):
    date_file_list = sorted(date_file_list, key=lambda date_file: date_file.date)
    return date_file_list

def truncate_date_files(date_file_list, begin_date,end_date):
    truncated_date_list = []
    for ffile in date_file_list:
        if ffile.date<end_date and ffile.date>begin_date:
            truncated_date_list.append(ffile)
    return truncated_date_list

def parse_date_files(ipattern, inputlist, output_dir_format, base_url_format):
    """parses the filenames from the listing according to the given pattern."""
    outputlist = []
    pattern = re.compile( ipattern )

    for ffile in inputlist:
        try:
            ffile = str(ffile)
            match = pattern.search(ffile)
            date_args = map(int, match.groups())
            if date_args[0]<50:
                date_args[0] = date_args[0]+2000
            if date_args[0]>=50 and date_args[0]<100:
                date_args[0] = date_args[0] + 1900
            date_toadd = datetime.datetime( *date_args )
            output_dir = date_toadd.strftime(output_dir_format )
            url = date_toadd.strftime( base_url_format )
            outputlist.append( DateFile(date_toadd, ffile, output_dir +"/"+ ffile, url + "/" + ffile) )
            match = None
        except:
            print "File does not match pattern\n file : \n" + str(ffile) + "\npattern:\n" + str(ipattern)
    return outputlist

def mkdirp(newdir):
    """Alternate mkdir
        - if dir already exists, silently complete
        - if regular file in the way, raise an exception
        - if parent directory does not exist, make them
    """
    if os.path.isdir(newdir):
        pass
    elif os.path.isfile(newdir):
        logging.error( "Trying to create a directory with a new \
                        which is already a file. The name is " + newdir )
        raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)
    else:
        head, tail = os.path.split(newdir)
        if head and not os.path.isdir(head):
            mkdirp(head)
        if tail:
            try:
                os.mkdir(newdir)
            except:
                logging.error( "Failed to create directory " + newdir )

def get_recursive_file_list(rootdir):
    """Get all files in a given directory recursively."""
    fileList = []
    for root, subFolders, files in os.walk(rootdir):
        for file in files:
            f = os.path.join(root,file)
            fileList.append(f)
    return fileList

def move( output_files, destdir, tempdir ):
    for output_file in output_files:
        tmp_file = output_file.replace( destdir, tempdir )
        if not os.path.isdir( os.path.dirname(output_file) ):
            mkdirp( os.path.dirname(output_file) )
        try:
            logging.debug("Moving " + tmp_file + " to " + output_file )
            os.rename(tmp_file, output_file)
        except:
            logging.error("The file " + tmp_file + "could not be moved to " + output_file)
            raise
