#!/swhv/python/bin/python
from __future__ import division # confidence high

import numpy
from astropy.io import fits
import sys
import urllib2
import json
import math
import argparse
import os.path
import sidcutil.fileutil as fileutil
import socket
from fits2fitsmodule.fits2fits_config import config
import logging
import sunpy
import fits2fitsmodule.detect_circle_alt as dc
from skimage.transform import rotate, warp, SimilarityTransform
import scipy
import scipy.special
import sunpy.coordinates

def get_P_angle_degrees( date ):
    """Returns the P-angle in radians."""
    try:
        url = 'http://swhv.oma.be/transform?utc=' + date + '&from_ref=GSEQ&to_ref=EARTH_FIXED&kind=angle'
        response = urllib2.urlopen( url, timeout = 30 )
        response_array = json.loads( response.read() ) [0]
        p = response_array[response_array.keys()[0]][2] * 180. / math.pi
    except Exception as e:
        logging.error("The p-angle could not be fetched from our service" + str(e))
        raise
    logging.debug("Successfully got the p-angle.")
    return p


def get_distance( date, object = "EARTH" ):
    """Returns the distance in meters between earth and sun."""
    try:
        logging.debug("Fetching the distance from swhv.oma.be")
        url = 'http://swhv.oma.be/position?observer=' + object + '&target=SUN&ref=J2000&abcorr=LT%2BS&utc=' + date + '&kind=latitudinal'
        response = urllib2.urlopen( url, timeout = 30 )
        response_array = json.loads( response.read() )["result"][0]
        print response_array
        distance = response_array[response_array.keys()[0]][0]*1000
        logging.debug("Successfully got the distance from swhv.oma.be")
    except Exception as e:
        logging.error("The distance could not be fetched from our service" + str(e))
        logging.debug("Compute distance with sunpy")
        distance = sunpy.coordinates.get_sunearth_distance(date).value*149597870700
    return distance

class Fits2fits():
    def __init__(self, input_file, output_file, fitstype, config_dict, args):
        self.args = args
        self.hdulist = None
        self.input_file = input_file
        self.output_file = output_file
        self.hdulist = fits.open(self.input_file, memmap=False)
        self.config_dict = config_dict
        if 'hdu_list_number' in self.config_dict:
            self.hdu = self.hdulist[self.config_dict['hdu_list_number']]
            self.prihdr = self.hdu.header
        else:
            self.hdu = self.hdulist[0]
            self.hdu.verify('silentfix' if args['verbose']==False else 'fix')
            self.prihdr = self.hdu.header
        if 'filename_prepend' in config_dict:
            self.filename_prepend = config_dict['filename_prepend']
        else:
            self.filename_prepend = None
        self.fitstype = fitstype
    def insert_keywords( self ):
        logging.debug("Inserting fits keywords")
        if self.fitstype == 'gong_magnetogram':
            date =  self.prihdr['DATE-OBS']
            self.prihdr['DATE-OBS'] = date[0:4]+"-"+date[5:7]+"-"+date[8:10]+'T'+self.prihdr['TIME-OBS']
            date =  self.prihdr['DATE-OBS']
        if self.fitstype == 'gong_farside':
            date =  self.prihdr['TIME0']
            self.prihdr['DATE-OBS'] = date[0:4]+"-"+date[5:7]+"-"+date[8:10]+'T'+date[11:13]+":"+date[14:16]+':00'
            date =  self.prihdr['DATE-OBS']
        if self.fitstype == 'rob_uset':
            date =  self.prihdr['DATE-OBS']
            self.prihdr['DATE-OBS'] = date[6:10]+"-"+date[3:5]+"-"+date[0:2]+'T'+self.prihdr['TIME']
            date =  self.prihdr['DATE-OBS']
        if 'DATE-OBS' in self.prihdr:
            date =  self.prihdr['DATE-OBS']
            self.prihdr['DATE_OBS'] = self.prihdr['DATE-OBS']
        if 'DATE_OBS' in self.prihdr:
            date =  self.prihdr['DATE_OBS']
            self.prihdr['DATE-OBS'] = self.prihdr['DATE_OBS']
        if not 'DSUN_OBS' in self.prihdr:
            self.prihdr.set('DSUN_OBS', get_distance(date))
        self.handle_add_overwrite()
        self.handle_add_if_not_exist()
        if not 'CDELT1' in self.prihdr:
            if 'FNDLMBMI' in self.prihdr and 'SOLAR-R' in self.prihdr:
                cdelt = self.prihdr['SOLAR-R']/self.prihdr['FNDLMBMI']
                self.prihdr.set('CDELT1', cdelt)
                self.prihdr.set('CDELT2', cdelt)
            elif 'FNDLMBMI' in self.prihdr and 'RADIUS' in self.prihdr:
                cdelt = self.radians_to_arcseconds(self.prihdr['RADIUS'])/self.prihdr['FNDLMBMI']
                self.prihdr.set('CDELT1', cdelt)
                self.prihdr.set('CDELT2', cdelt)
            else:
                center, radius = dc.find_circle(self.input_file)
                self.prihdr.set( 'CDELT1', self.radians_to_arcseconds(math.atan2( 6.955e8, self.prihdr['DSUN_OBS']))/radius )
                self.prihdr.set( 'CDELT2', self.prihdr['CDELT1'] )
                self.prihdr.set( 'CRPIX1', center[1]+1 )# (self.prihdr['NAXIS1'] - 1 - center[1]) + 1 )
                self.prihdr.set( 'CRPIX2', center[0]+1 )# (self.prihdr['NAXIS2'] - 1 - center[0]) + 1 )

        if 'WAVELNTH' in self.prihdr:
            print self.fitstype
            if self.fitstype == 'gong_farside':
                self.prihdr.set('WAVELNTH', "farside")
            elif self.fitstype == 'gong_magnetogram':
                self.prihdr.set('WAVELNTH', "magnetogram")
            else:
                self.prihdr.set('WAVELNTH', str(int(float(self.prihdr['WAVELNTH']))))            
        self.prihdr.set('CDELT1', abs(self.prihdr['CDELT1']))
        self.prihdr.set('CDELT2', abs(self.prihdr['CDELT2']))
        self.handle_eE()

    def handle_add_overwrite( self ):
        if 'add_overwrite' in self.config_dict:
            for key in self.config_dict['add_overwrite'].keys():
                self.prihdr.set(key, self.config_dict['add_overwrite'][key])

    def handle_add_if_not_exist(self):
        if 'add_if_not_exist' in self.config_dict:
            for key in self.config_dict['add_if_not_exist'].keys():
                if not key in self.prihdr:
                    self.prihdr.set(key,self.config_dict['add_if_not_exist'][key])
    def handle_eE(self):
        if 'change_eE' in self.config_dict:
            for key in self.config_dict['change_eE']:
                if key in self.prihdr:
                    float = self.prihdr[key]
                    del self.prihdr[key]
                self.prihdr.set(key, float)
                logging.debug(self.prihdr[key])

    def remove_keywords(self):
        deletelist = []
        for key in deletelist:
            if key in self.prihdr:
                logging.debug('Removing key ' +  key)
                del self.prihdr[key]

    def check_minimal_keywords(self):
        assert( 'CDELT1' in self.prihdr )
        assert( 'CDELT2' in self.prihdr )
        assert( 'CRPIX1' in self.prihdr )
        assert( 'CRPIX2' in self.prihdr )
        assert( 'DETECTOR' in self.prihdr )
        assert( 'INSTRUME' in self.prihdr )
        assert( 'DSUN_OBS' in self.prihdr )
        assert( 'DATE-OBS' in self.prihdr )
        assert( 'DATE_OBS' in self.prihdr )
        assert( 'WAVELNTH' in self.prihdr )


    def radians_to_arcseconds(self, radians):
        return radians * 206264.806247

    def arcseconds_to_radians(self, arcseconds):
        return arcseconds / 206264.806247

    def compute_distance( self, array ):
        """Given an array of coordinates in kilometers, return the Euclidean length of the array in meters."""
        sum = 0
        for point in array:
            sum += point*point
        return 1000*math.sqrt( sum )


    def writefile( self ):
        filename = self.output_file
        if 'filename_prepend' in self.config_dict:
            filename_prepend = self.config_dict['filename_prepend']
            filename = os.path.basename(self.output_file)
            dirname = os.path.dirname(self.output_file)
            filename = dirname + "/"  + filename_prepend + filename
        if os.path.exists(self.output_file):
            os.remove(self.output_file)
        dirname = os.path.dirname(self.output_file)
        fileutil.mkdirp(dirname)
        self.hdu.writeto(filename,output_verify='silentfix' if self.args['verbose']==False else 'fix', checksum = 'remove', overwrite=True)


    def change_data( self ):
        if 'image_type' in self.config_dict:
            self.hdulist[self.config_dict['hdu_list_number']].data = numpy.copy(self.hdulist[self.config_dict['hdu_list_number']].data[self.config_dict['image_type']][:][:])
        if 'rotate_old' in self.config_dict and self.config_dict['rotate']:
            mmax = numpy.max(self.hdulist[self.config_dict['hdu_list_number']].data)
            ha = self.hdulist[self.config_dict['hdu_list_number']].data/mmax
            ha = rotate(ha, self.prihdr['ANGLE'], order=3)
            ha = ha*mmax
            self.hdulist[self.config_dict['hdu_list_number']].data = ha
        if 'rotate_kanzelhoehe' in self.config_dict and self.config_dict['rotate_kanzelhoehe']:
            cp1 = (self.prihdr['CENTER_X']-1)
            cp2 = (self.prihdr['CENTER_Y']-1)

            ha = self.hdulist[self.config_dict['hdu_list_number']].data
            mmax = numpy.max(ha)
            ha = ha/mmax
            rows, cols = ha.shape[0]-1, ha.shape[1]-1
            angle = self.prihdr['ANGLE']
            translation_crpix = numpy.array((cp1, cp2))
            translation_to_center = (numpy.array((cols, rows))-1.0) / 2.
            tform1 = SimilarityTransform(translation=-translation_crpix)
            tform2 = SimilarityTransform(rotation=numpy.deg2rad(angle))
            tform3 = SimilarityTransform(translation=translation_to_center)
            tform = tform1 + tform2 + tform3
            ha = warp(ha, tform, output_shape=None, order=3, mode='constant', cval=0.)
            ha = ha*mmax
            self.hdulist[self.config_dict['hdu_list_number']].data = ha
            self.prihdr['CRPIX1'] = (self.prihdr['NAXIS1']-1)/2.0 +1
            self.prihdr['CRPIX2'] = (self.prihdr['NAXIS2']-1)/2.0 +1
        if 'center' in self.config_dict and self.config_dict['center']:
            cp1 = (self.prihdr['CRPIX1']-1)
            cp2 = (self.prihdr['CRPIX2']-1)

            ha = self.hdulist[self.config_dict['hdu_list_number']].data
            rows, cols = ha.shape[0]-1, ha.shape[1]-1
            translation_crpix = numpy.array((cp1, cp2))
            translation_to_center = (numpy.array((cols, rows))-1.0) / 2.
            translation =  -translation_crpix+translation_to_center
            hna = scipy.ndimage.shift(ha, (translation[1],translation[0]), order=3, prefilter=True, mode='constant', cval=0.,)
            self.hdulist[self.config_dict['hdu_list_number']].data = hna
            self.prihdr['CRPIX1'] = (self.prihdr['NAXIS1']-1)/2.0 +1
            self.prihdr['CRPIX2'] = (self.prihdr['NAXIS2']-1)/2.0 +1
        if 'rotate_and_center' in self.config_dict and self.config_dict['rotate_and_center']:
            cp1 = (self.prihdr['CRPIX1']-1)
            cp2 = (self.prihdr['CRPIX2']-1)

            ha = self.hdulist[self.config_dict['hdu_list_number']].data
            rows, cols = ha.shape[0]-1, ha.shape[1]-1
            translation_crpix = numpy.array((cp1, cp2))
            translation_to_center = (numpy.array((cols, rows))-1.0) / 2.
            translation =  -translation_crpix+translation_to_center
            hna = scipy.ndimage.shift(ha, (translation[1],translation[0]), order=3, prefilter=True, mode='constant', cval=0.,)
            angle = get_P_angle_degrees(self.prihdr['DATE-OBS'])
            hba = numpy.zeros(hna.shape)
            scipy.ndimage.rotate(hna,  angle, output=hba, reshape=False, order=3, prefilter=True, mode='constant', cval=0.,)
            self.hdulist[self.config_dict['hdu_list_number']].data = hba
            self.prihdr['CRPIX1'] = (self.prihdr['NAXIS1']-1)/2.0 +1
            self.prihdr['CRPIX2'] = (self.prihdr['NAXIS2']-1)/2.0 +1

    def __del__( self ):
        if ( not self.hdulist==None ):
            self.hdulist.close()

def parse_arguments():
    parser = argparse.ArgumentParser( description='This programs adds some extra SWHV keywords to the given fitsfile.' )
    parser.add_argument( '-i','--input_file', help = 'The name of the inputfile', required = True )
    parser.add_argument( '-o','--output_file', help = 'The name of the outputfile', required = True )
    parser.add_argument( '-t','--type', help = 'The type of the fitsfile that needs processing.', required = True )
    parser.add_argument( '-v','--verbose', help = 'Verbose info logging', required = False )
    args = vars(parser.parse_args())
    return args

def process_command():
    args = parse_arguments()
    fitstype = args['type']
    if fitstype in config:
        config_helper = config[fitstype]
        if not 'hdu_list' in config_helper:
            logging.error('The config file for type ' + fitstype + ' is corrupt. Please add hdu_list.')
        else:
            hdu_list = config_helper['hdu_list']
        for hdu in hdu_list:
            fits2fits = Fits2fits( args['input_file'], args['output_file'], fitstype, hdu, args)
            fits2fits.insert_keywords()
            fits2fits.remove_keywords()
            fits2fits.change_data()
            fits2fits.check_minimal_keywords()
            fits2fits.writefile()
            del fits2fits
    else:
        logging.error('The given type ' + fitstype + ' is not in the config file.')

if __name__ == '__main__':
    process_command()
    #print get_P_angle_degrees("2015-01-20T12:12:12")
    #print get_distance("2015-01-20T12:12:12")
