#!/swhv/python/bin/python
import datetime
import sys
import scipy.ndimage.morphology as morpheus
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
import os
import scipy.signal as sig
import sidcutil.datautil as du
import sidcutil.fileutil as fileutil
import argparse

fig = plt.figure(frameon=False)

def plot_part_of_image(v, start, end,filename, dpi=96):
    fig.set_size_inches((end-start)/dpi, v.shape[0]/dpi)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    v = v.astype("float32")
    v = 255-v
    ax.imshow(v[:,int(start):int(end)], aspect='auto',cmap="Greys", vmin=0, vmax=256, interpolation="nearest")
    plt.savefig(filename, bbox_inches=0, dpi=dpi)
    plt.clf()

def plot_raw_image(v,filename="test",dpi = 96):
    #print v.shape[1], v.shape[0]
    next_start = 0
    next_end = math.pow(2,14)-1
    count=0
    filename = filename + "_{:05d}.png"
    while next_end < v.shape[1]:
        plot_part_of_image(v, next_start, next_end,filename.format(count), dpi=dpi)
        next_start = next_end + 1
        next_end = next_start + math.pow(2,14) - 1
        count = count + 1
    plot_part_of_image(v, next_start, v.shape[1],filename.format(count), dpi=dpi)

def datetime_to_timestamp(array):
    new_array = []
    help_time = datetime.datetime(1970,1,1)
    for i in array:
        new_array.append( (i-help_time).total_seconds() )
    return new_array

def timestamp_to_datetime(array):
    new_array = []
    help_time = datetime.datetime(1970,1,1)
    for i in array:
        new_array.append( help_time +datetime.timedelta( seconds = i ) )
    return new_array


def calibrate_array(v, datamin):
    linep=np.percentile(v,90, axis=1)
    linepm=np.percentile(v,100, axis=1)
    linemax=np.amax(v, axis=1)
    for i in range(v.shape[0]):
        for j in range(v.shape[1]):
            v[i,j] = max(v[i,j], linep[i])
            v[i,j] = min(v[i,j], linepm[i])
    diff = (linepm-linep)
    diff = diff + 1.
    v = (v-linep[:,None])/diff[:,None]*255
    return v

def define_closest_smaller_in_bin (bins, value):
    where_bigger = np.where(bins > value)
    if len(where_bigger[0]) > 0:
        min_bigger = np.min(where_bigger)
        if min_bigger > 0:
            return bins[min_bigger-1]
        else:
            return bins[0]
    else:
        return None


def define_closest_bigger_in_bin (bins, value):
    # do check if there is no element
    where_smaller = np.where(bins < value)
    if len(where_smaller[0]) > 0:
        max_smaller = np.max(where_smaller)
        if max_smaller < len(bins)-2:
            return bins[max_smaller + 1]
        else:
            return bins[len(bins)-1]
    else:
        return None

def correct_date (date, time):
    deltat = datetime.timedelta(0)
    t_s = int(time[6:8])
    t_m = int(time[3:5])
    t_h = int(time[0:2])
    if t_s >= 60:
        t_m = t_m + t_s / 60
        t_s = t_s % 60
    if t_m >= 60:
        t_h = t_h + t_m / 60
        t_m = t_m % 60
    if t_h >= 24:
        deltat = deltat + datetime.timedelta(seconds = (t_h / 24) * 3600 * 24)
        t_h = t_h % 24
    timestr = '{:02d}:{:02d}:{:02d}'.format(t_h, t_m, t_s)
    end_datetime_obs = datetime.datetime.strptime(date + " " + timestr, "%Y/%m/%d %H:%M:%S")
    return end_datetime_obs + deltat

def morph_gray_opening (v):
    return morpheus.grey_opening(v,size=(5,5))

def process_file(filename,delta_freq,delta_time,time_bins,frequency_bins):
    print "Processing: " +filename
    try:
        temp_call = fits.open(filename,ignore_missing_end=True)
    except Exception as inst:
        print "opening:" + filename + " did not succeeded"
        print inst
        return None
    start_datetime_obs = correct_date(temp_call[0].header['DATE-OBS'], temp_call[0].header['TIME-OBS'])
    end_datetime_obs = correct_date(temp_call[0].header['DATE-END'], temp_call[0].header['TIME-END'])
    try:
        v = np.asarray(temp_call[0].data)
    except:
        print "Processing: " + filename + " did not succeeded probably truncated file"
        temp_call.close()
        return None
    closest_start_date = define_closest_smaller_in_bin(time_bins, datetime_to_timestamp([start_datetime_obs]))
    closest_end_date = define_closest_bigger_in_bin(time_bins, datetime_to_timestamp([end_datetime_obs]))
    try: 
        data_time_array_help = temp_call[1].data[0][0]
        data_freq_array_help = temp_call[1].data[0][1]
    except:
        print "time or frequency data is corrupt"
        temp_call.close()
        return None
    data_time_array = []
    closest_start_frequency = define_closest_smaller_in_bin(frequency_bins,data_freq_array_help[-1])
    closest_end_frequency = define_closest_bigger_in_bin(frequency_bins, data_freq_array_help[0] )
    if closest_start_date != None and closest_end_date != None and closest_start_frequency != None and closest_end_frequency != None and closest_end_date - closest_start_date > delta_time:
        datamin = temp_call[0].header['DATAMIN']
        datamax = temp_call[0].header['DATAMAX']
        #Better to do calibration per dataset in a longer time duration array
        v = calibrate_array(v, datamin)
        v = morph_gray_opening(v)
        v = np.flipud(v)
        v = v.transpose()
        for point in data_time_array_help:
            data_time_array.append( datetime.timedelta(seconds = point) + start_datetime_obs)
            #data_time_array.append( point + (datetime_to_timestamp([start_datetime_obs]	))[0] )
        orig_times = np.asarray(datetime_to_timestamp(data_time_array))
        #orig_times = np.asarray(data_time_array)
        #time_bins = np.arange(orig_times[0], orig_times[-1], delta_time)
        time_bins = np.arange(closest_start_date, closest_end_date , delta_time)
        v = median_bin(  orig_times, time_bins, v)
        v = v.transpose()
        #freq_bins = np.arange( (int)(data_freq_array_help[-1]), (int)(data_freq_array_help[0]), delta_freq)
        freq_bins = np.arange(closest_start_frequency, closest_end_frequency, delta_freq)
        v = median_bin( data_freq_array_help[::-1], freq_bins, v)
        v = v.transpose()
        temp_call.close()
        return(time_bins[0], time_bins[1], freq_bins[0], freq_bins[-1], v, time_bins, freq_bins, time_bins[-1])
    else :
        temp_call.close()
        return None

def median_bin( orig_ax, bins, data_array ):
    """orig_ax contains the tickpoints of the original axis; bins contains the tickpoints of a bin.
    Given two subsequent points a,b of the bin, this function medianizes all tickpoints int the orig_ax that lie between a and b
    If a or b is exactly a tickpoint, it is counted in the entire median.
    In case a multidimensional data_array is given, the medianization is done in the first dimension.
    Example:
    v = np.random.rand(11,11)
    #v = np.ones((11,11))
    #v = np.identity(11)
    #Binning in the rows
    v = median_bin(  np.arange(11), np.array([0,0.1,4,9]), v)
    print v
    #Transpose to bin in the columns
    v = v.transpose()
    print v
    v = median_bin( np.arange(11), np.array([0,4,9]), v)
    print v
    #Transpose again to go back to original
    v = v.transpose()
    """
    bins_size = bins.size
    index = 0
    orig_ax_size = orig_ax.shape[0]
    count = 0
    shape = []
    for i in data_array.shape:
        if count==0:
            count = 1
            shape.append(bins_size)
        else:
            shape.append(i)
    binned_array = np.zeros(shape)
    for i in range(bins_size):
        help_data_index_arr = []
        while( index < orig_ax_size and orig_ax[index] < bins[i] ):
            help_data_index_arr.append(index)
            index = index + 1
        if index < orig_ax_size and orig_ax[index] == bins[i]:
            help_data_index_arr.append(index)
        if len(help_data_index_arr)>0:
            binned_array[i] = np.median( data_array[help_data_index_arr], axis=0)
    return binned_array


def process_list(file_list, start_time, end_time, delta_time_sec, start_frequency, end_frequency, delta_frequency):
    start_datetime = datetime.datetime.strptime(start_time,"%Y-%m-%dT%H:%M:%S")
    end_datetime = datetime.datetime.strptime(end_time,"%Y-%m-%dT%H:%M:%S")
    #enter correct time/freq bins here
    time_bins = np.arange((datetime_to_timestamp([start_datetime]))[0],(datetime_to_timestamp([end_datetime]))[0],delta_time_sec)
    freq_bins = np.arange(start_frequency, end_frequency, delta_frequency)
    #freq_bins=np.logspace(np.log10(start_frequency), np.log10(end_frequency),num=200)
    tot_array = np.zeros((time_bins.size, freq_bins.size))
    tot_array_amounts = np.zeros((time_bins.size, freq_bins.size))
    for filename in file_list:
        vv = process_file(filename,delta_frequency , delta_time_sec, time_bins,freq_bins)
        if vv == None:
            continue
        wheres1 = np.where(np.logical_and(time_bins >= vv[0], time_bins <= vv[7]))
        min_time_in_global_bins = time_bins[np.min(wheres1)]
        max_time_in_global_bins = time_bins[np.max(wheres1)]
        wheres2 = np.where(np.logical_and(vv[5] >= min_time_in_global_bins, vv[5] <= max_time_in_global_bins))
        first_time = np.where(time_bins <= vv[0])[0][-1]
        first_freq = np.where(freq_bins <= vv[2])[0][-1]
        tot_array[ first_time:first_time+vv[4].shape[0], first_freq:first_freq+vv[4].shape[1]] = np.maximum(tot_array[ first_time:first_time+vv[4].shape[0], first_freq:first_freq+vv[4].shape[1]], vv[4])#/np.max(vv[4])
        del(vv)
    #tot_array_amounts[np.where(tot_array_amounts == 0)] = 1
    #tot_array = tot_array/tot_array_amounts
    mmax = np.amax(tot_array, axis = 0)
    for ind in range(1,tot_array.shape[1]):
        if mmax[ind]==0:
            pass
            #tot_array[:, ind] = tot_array[:,ind-1]
    return tot_array.transpose()

def get_input_from_database(input_directory, startdate, enddate, station):
    temp_start = startdate - datetime.timedelta(hours=1)
    temp_end = enddate + datetime.timedelta(hours=1)
    out = list()
    existing_list = fileutil.get_recursive_file_list(input_directory)
    existing_datefiles = fileutil.parse_date_files(station+r'.*_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_[0-9]{2}.fit.gz', existing_list,input_directory+ "/%Y/%m/%d", "")
    #existing_datefiles = fileutil.truncate_date_files(existing_datefiles, fileutil.parse_date(temp_start), temp_end)
    existing_datefiles = fileutil.truncate_date_files(existing_datefiles,temp_start, temp_end)
    for el in existing_datefiles:
        out.append(el.path)
    return out

def create_fits_file(data_array, args):
    hdu = fits.PrimaryHDU(data_array)
    hdulist = fits.HDUList([hdu])
    
    #Enter filename and header keywords here
    hduhdr = hdulist[0].header
    hduhdr['DATE-OBS'] = args['starttime']
    hduhdr['TELESCOP'] = "ROB-Humain"
    hduhdr['INSTRUME'] = "CALLISTO"
    hduhdr['DETECTOR'] = "CALLISTO"
    hduhdr['WAVELNTH'] = "RADIOGRAM"
    hduhdr['DATE-END'] = args['endtime']
    hduhdr['TIMEDELT'] = args['deltatime']
    hduhdr['STARTFRQ'] = args['startfrequency']
    hduhdr['END-FREQ'] = args['endfrequency']
    hduhdr['FREQDELT'] = args['deltafrequency']
    hduhdr['CDELT1']=1
    hduhdr['CDELT2']=1
    hduhdr['CRPIX1']=0
    hduhdr['CRPIX2']=0
    
    datetime_start = datetime.datetime.strptime(args['starttime'],"%Y-%m-%dT%H:%M:%S")
    datetime_end = datetime.datetime.strptime(args['endtime'],"%Y-%m-%dT%H:%M:%S")
    format = "%Y%m%d_%H%M%S"
    hdulist.writeto(args['output_dir']+"/"+datetime.datetime.strftime(datetime_start, format)+"_"+datetime.datetime.strftime(datetime_end,format)+".fits",overwrite = True)

def parse_arguments():
    args = dict()
    parser = argparse.ArgumentParser( description='This program combines several callisto files. Given the start and end time, start and end frequency, delta time and delta frequency it will create new fits files.\nUsage:' )
    parser.add_argument( '-i','--input', help = 'Comma seperated list of Input files', required = False ) #Will be deleted, the input will be fetched from the database.
    parser.add_argument( '-d','--input_directory', help = 'The directory where the inputfiles can be found', required = True)
    parser.add_argument( '-o','--output_dir', help = 'Output directory', required = True ) # Will be the temp directry given by the runner, but we don't care.
    parser.add_argument( '-t','--starttime', help = 'Start time of the interval', required = True)
    parser.add_argument( '-T','--endtime', help = 'End time of the interval', required = True)
    parser.add_argument( '-f','--startfrequency', help = 'Start frequency of the interval', required = True)
    parser.add_argument( '-F','--endfrequency', help = 'End frequency of the interval', required = True)
    parser.add_argument( '-u','--deltatime', help = 'Step in time in the new image', required = True)
    parser.add_argument( '-g','--deltafrequency', help = 'Step in frequency in the new image', required = True)
    args = vars(parser.parse_args())
    if args['input'] == None:
        args['input'] = get_input_from_database(args['input_directory'],datetime.datetime.strptime(args['starttime'],"%Y-%m-%dT%H:%M:%S"),datetime.datetime.strptime(args['endtime'],"%Y-%m-%dT%H:%M:%S"), '')
    return args

def main():    
    args = parse_arguments()
    n=process_list((args['input']),args['starttime'],args['endtime'],float(args['deltatime']),float(args['startfrequency']),float(args['endfrequency']),float(args['deltafrequency']))
    n[np.isnan(n)] = 0
    create_fits_file(n.astype("float32"), args)

if __name__ == "__main__":
    main()
