import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits

def find_circle(filename, iter = 2, show_image = False, cut=0, reverse = False):
    fts = fits.open(filename)
    im = np.abs(fts[0].data)
    im = im[cut:im.shape[0]-cut,cut:im.shape[0]-cut]
    fts.close()
    for i in range(2):
        av = np.average(im)
        im[im>av]=av
    av = np.average(im)
    if reverse:
        im[im<av]=1
        im[im>av]=0
    else:
        im[im<av]=0
        im[im>av]=1
    hs = np.sum(im, axis = 1)
    vs = np.sum(im, axis = 0)
    hmax = np.argmax(hs)
    vmax = np.argmax(vs)
    rad1 = hs
    rad1[rad1>=1]=1
    i=0
    while(rad1[i]==0): 
        i = i+1
    hleft = i
    i = im.shape[0] -1 
    while(rad1[i]==0): 
        i = i-1
    hright = i
    hmax = hleft + (hright - hleft) /2.0
    radi1 = np.sum(rad1)/2.0
    rad2 = vs
    rad2[rad2>=1]=1
    i=0
    while(rad2[i]==0): 
        i = i+1
    vleft = i
    i = im.shape[1]-1
    while(rad2[i]==0): 
        i = i-1
    vright = i
    vmax = vleft + (vright - vleft) /2.0
    radi2 = np.sum(rad2)/2.0
    rad = (radi1 +radi2)/2.0
    im[hleft:hright,int(vmax)-2:int(vmax)+2]=2
    im[int(hmax)-2:int(hmax)+2,vleft:vright]=2
    if show_image:
        plt.imshow(im, cmap=plt.cm.gray)
        circle1=plt.Circle((vmax,hmax),rad,ec='r',fc='none')
        fig = plt.gcf()
        fig.gca().add_artist(circle1)
        plt.title('noisy image', fontsize=20)
        plt.show()
    return (hmax,vmax), rad

if __name__ == "__main__":
    find_circle("/Users/freekv/Downloads/bbbqa131014t1454.fits", show_image = True, iter=2, cut=0, reverse = True)
    find_circle("/data/fits2fits/rob_uset/2014/02/02/UCH20140202091015.FTS", show_image=True)
    #find_circle("/Users/freekv/Downloads/UCH20130304123658.FTS", show_image = True)
    #find_circle("/Users/freekv/Downloads/UCH20140202091015.FTS", show_image = True)

