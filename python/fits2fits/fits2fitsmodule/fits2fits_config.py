#observatory
#self.prihdr.set('TELESCOP', "NSO")
#instrument
#self.prihdr.set('INSTRUME', "VSM")
#detector
#self.prihdr.set('DETECTOR', "VSM")
#measurement
#self.prihdr.set('WAVELNTH', "854_2_"+ lookup[0:5])

config = {
    'gong_halpha': {
        'hdu_list': [
            {
                'hdu_list_number': 1,
                'filename_prepend': '',
                'add_if_not_exist': {
                 },
                 'add_overwrite': {
                    'TELESCOP': "NSO-GONG",
                    'INSTRUME': "GONG",
                    'DETECTOR': "H-alpha",
                    'WAVELNTH': 6562,
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 }
            }
        ],
    },
    'gong_magnetogram' : {
        'hdu_list' : [
            {
                'hdu_list_number': 0,
                 'center': True,
                 'add_overwrite': {
                    'ORIGIN': "NSO-GONG",
                    #observatory
                    'TELESCOP': "NSO-GONG",
                    #instrument
                    'INSTRUME': "GONG",
                    #detector
                    'DETECTOR': "GONG",
                    #measurement
                    'WAVELNTH': "magnetogram"
                 }
            }
        ],
    },
    'gong_farside' : {
        'hdu_list' : [
            {
                 'add_overwrite': {
                    'ORIGIN': "NSO-GONG",
                    #observatory
                    'TELESCOP': "NSO-GONG",
                    #instrument
                    'INSTRUME': "GONG",
                    #detector
                    'DETECTOR': "GONG",
                    #measurement
                    'WAVELNTH': "farside",
                    'CRPIX1': 100.5,
                    'CRPIX2': 100.5,
                    'CDELT1':11.1,
                    'CDELT2':11.1,
                 }
            }
        ],
    },
    'solis_v82': {
        'hdu_list': [
            {
                'hdu_list_number': 0,
                'filename_prepend': 'corefluxdens',
                'image_type': 0,
                'image_type_name': 'CoreFluxDens',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "CoreFluxDens",
                    #measurement
                    'WAVELNTH': "8542"
                 },
                 'change_eE': ['TILT1', 'CURV1', 'CURV2', 'CURV3', 'CURV4']
            },
            {
                'hdu_list_number': 0,
                'filename_prepend': 'corewingintensity',
                'image_type': 2,
                'image_type_name': 'CoreWingIntensity',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "CoreWingInt",
                    #measurement
                    'WAVELNTH': "8542"
                 },
                 'change_eE': ['TILT1', 'CURV1', 'CURV2', 'CURV3', 'CURV4']
            }
        ],
    },
    'solis_v95': {
        'hdu_list': [
            {
                'hdu_list_number': 0,
                'filename_prepend': 'strength',
                'image_type': 0,
                'image_type_name': 'Strength',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "Strength",
                    #measurement
                    'WAVELNTH': "6302"
                 }
            },
            {
                'hdu_list_number': 0,
                'filename_prepend': 'inclination',
                'image_type': 1,
                'image_type_name': 'Inclination',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "Inclination",
                    #measurement
                    'WAVELNTH': "6302"
                 }
            },
            {
                'hdu_list_number': 0,
                'filename_prepend': 'azimuth',
                'image_type': 2,
                'image_type_name': 'Azimuth',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "Azimuth",
                    #measurement
                    'WAVELNTH': "6302"
                 }
            },
            {
                'hdu_list_number': 0,
                'filename_prepend': 'fillfactor',
                'image_type': 3,
                'image_type_name': 'fillfactor',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "FillFactor",
                    #measurement
                    'WAVELNTH': "6302"
                 }
            },
            {
                'hdu_list_number': 0,
                'filename_prepend': 'intensity',
                'image_type': 4,
                'image_type_name': 'Intensity',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "Intensity",
                    #measurement
                    'WAVELNTH': "6302"
                 }
            },
        ],
    },
    'solis_v22': {
        'hdu_list': [
            {
                'hdu_list_number': 0,
                'image_type' : 0,
                'filename_prepend':'',
                'image_type_name': 'Chi-Squared',
                'add_if_not_exist': {
                    'CRPIX1': 1024.5,
                    'CRPIX2': 1024.5,
                 },
                 'add_overwrite': {
                    'ORIGIN': "NSO-SOLIS",
                    #observatory
                    'TELESCOP': "NSO-SOLIS",
                    #instrument
                    'INSTRUME': "VSM",
                    #detector
                    'DETECTOR': "Intensity",
                    #measurement
                    'WAVELNTH': "1083"
                 }
            },
        ],
    },
    'rob_uset' : {
        'hdu_list' : [
            {
                'hdu_list_number': 0,
                'rotate_and_center': True,
                'add_overwrite': {
                    'ORIGIN'  : "ROB-USET",
                    #observatory
                    'TELESCOP': "ROB-USET",
                    #instrument
                    'INSTRUME': "H-alpha",
                    #detector
                    'DETECTOR': "H-alpha",
                    #measurement
                    'WAVELNTH': "6562"
                }
            }
        ],
    },
    'kanzelhoehe' : {
        'hdu_list' : [
            {
                'hdu_list_number': 0,
                'rotate_kanzelhoehe': True,
                 'add_overwrite': {
                    'ORIGIN'  : "Kanzelhoehe",
                    #observatory
                    'TELESCOP': "Kanzelhoehe",
                    #instrument
                    'INSTRUME': "H-alpha",
                    #detector
                    'DETECTOR': "H-alpha",
                    #measurement
                    'WAVELNTH': "6562"
                 }
            }
        ],
    },
}
