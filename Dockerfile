FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y  && apt-get install -y \  
 curl \
 software-properties-common \
 supervisor \  
 nginx \  
 zlib1g-dev \
 php7.0-fpm \  
 php7.0-cli \  
 php7.0-curl \  
 php7.0-gd \  
 php7.0-mysql \  
 php7.0-memcached \  
 php7.0-mcrypt \
 php7.0-dev \
 default-libmysqlclient-dev \
 python python-pip \
 libjpeg-dev \
 libxml2-dev libxslt-dev \
 && echo -n > /var/lib/apt/extended_states \
 && rm -rf \
 /var/lib/apt/lists/* \  
 /usr/share/man/?? \ 
 /usr/share/man/??_* \
 /etc/nginx/conf.d/* \  
 /etc/nginx/sites-available/default


# Configure php-fpm
RUN mkdir /run/php
RUN sed -e 's/;daemonize = yes/daemonize = no/' -i /etc/php/7.0/fpm/php-fpm.conf  
RUN sed -e 's/;listen\.owner/listen.owner/' -i /etc/php/7.0/fpm/pool.d/www.conf  
RUN sed -e 's/;listen\.group/listen.group/' -i /etc/php/7.0/fpm/pool.d/www.conf
RUN sed -e 's/\/run/\/var\/run/' -i /etc/php/7.0/fpm/pool.d/www.conf

# Configure nginx to not run in daemonized mode
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Fix permissions
RUN usermod -u 1000 www-data \
 && groupmod -g 1000 www-data \
 && chown -Rf www-data:www-data /var/www/

VOLUME /data
RUN chown -R www-data /data
RUN chgrp -R www-data /data


# Configure nginx and Supervisor
ADD ./SWHV-HVORG/nginx-vhost.conf /etc/nginx/sites-available/default.conf  
RUN ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf
ADD ./SWHV-HVORG/supervisord.conf /etc/supervisor/conf.d/supervisor.conf

# PYTHON INSTALL
RUN pip install numpy && \
    pip install cython && \
    pip install pillow && \
    pip install lxml>=2.3 && \
    pip install glymur>=0.5.10 && \
    pip install jpylyzer>=1.18

ADD ./kakadu /kakadu
RUN cp /kakadu/libkdu_v7AR.so /usr/lib/
RUN cp /kakadu/kdu_transcode /usr/local/bin/

ADD ./external/hvJP2K /python/hvJP2K
RUN pip install /python/hvJP2K

ADD ./external/wait-for-it /wait-for-it
# Add hv.org api
ADD ./external/api /var/www/api
ADD ./SWHV-HVORG/Private.php /var/www/api/settings/Private.php
ADD ./SWHV-HVORG/Config.ini /var/www/api/settings/Config.ini

ADD ./SWHV-HVORG/scripts /scripts
RUN chown -R www-data /scripts
RUN chgrp -R www-data /scripts


CMD ["/usr/bin/supervisord"]  
